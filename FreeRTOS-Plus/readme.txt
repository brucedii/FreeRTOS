Directories:

+ The FreeRTOS-Plus/Source directory contains source code for some of the
  FreeRTOS+ components.  These subdirectories contain further readme files and
  links to documentation.

+ The FreeRTOS-Plus/Demo directory contains a demo application for every most of
  the FreeRTOS+ components.  Lots of the demo applications use the FreeRTOS

+ See http://www.freertos.org/plus

+ See http://www.freertos.org/plus1

